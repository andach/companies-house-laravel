# Laravel Companies House 

This Laravel Package implements an API client for the Companies House REST API. It can be used to look up information about companies registered in the United Kingdom. 

## Installation

To install, use the following to pull the package via Composer.

```
composer require andach/companies-house-laravel
```

Now register the Service Provider in config/app.php (not needed for Laravl 5.5+)

```
'providers' => [
    
    ...
    
    Andach\CompaniesHouse\CompaniesHouseServiceProvider::class,
],
```
And also add the alias to the same file.

```
'aliases' => [
    
    ...
    
    'CompaniesHouse' => Andach\CompaniesHouse\Facades\CompaniesHouse::class,
],
```
Finally publish the config file.
```
php artisan vendor:publish
```

## How to use?

```
use Andach\CompaniesHouse\Facades\CompaniesHouse;
```

#### Charges

```
CompaniesHouse::company('12345678')->listCharges();
CompaniesHouse::company('12345678')->findCharge('dJc5r4NI1d0nC3lPHoQ0ocSJGXY');
```

The list() function will return a standard object if at least one charge exists, and NULL if the company is invalid or there are no charges found for it. 

The find() function will return a standard object if the charge is found, and NULL if the charge or company is not found (or the chargeID is not for this company number). 

#### Companies

```
CompaniesHouse::company('12345678')->get();
CompaniesHouse::company('12345678')->registeredOfficeAddress();
CompaniesHouse::company('12345678')->officers();
CompaniesHouse::company('12345678')->ukEstablishments();
```

#### Filing History

```
CompaniesHouse::company('12345678')->listFilingHistory();
CompaniesHouse::company('12345678')->findFilingTransaction('MzE4MjE3NzM2MGFkaXF6a2N4');
```

#### Officers

```
?
```

#### Search

```
CompaniesHouse::search()->all('Ebury');
CompaniesHouse::search()->companies('Ebury');
CompaniesHouse::search()->officers('Ebury');
CompaniesHouse::search()->disqualified_officers('Ebury');
```

## Configuration

### Obtaining the CompaniesHouse API Key

 - You will need to register an application with Companies House by visiting https://developer.companieshouse.gov.uk/developer/applications.
 - Then get an API Key which can be used within your Laravel Application in config/companies.php.

## Companies House API (Beta release)

 - [Getting started](https://developer.companieshouse.gov.uk/api/docs/) 
 - [Authorisation](https://developer.companieshouse.gov.uk/api/docs/index/gettingStarted/apikey_authorisation.html)
 - [Rate Limitation](https://developer.companieshouse.gov.uk/api/docs/index/gettingStarted/rateLimiting.html)

## Questions
Feel free to submit an issue if you have any issues.

## Contributing
 - Fork it!
 - Create your feature branch: git checkout -b my-new-feature
 - Commit your changes: git commit -m 'Add some feature'
 - Push to the branch: git push -u origin my-new-feature
 - Submit a pull request - cheers!

## Tests
Pull requests will not generally be accepted without tests. This project runs with a docker container that can be used to run tests. Simply execute:

`docker-compose up -d`

In your terminal, then in the docker container, run:

`php vendor/bin/phpunit`

## Thanks
This is based off an old package ghazanfarmir/laravel-companies-house. 