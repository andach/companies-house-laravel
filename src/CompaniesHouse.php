<?php

namespace Andach\CompaniesHouse;

use Andach\CompaniesHouse\Http\Client;
use Andach\CompaniesHouse\Resources\Search;
use Andach\CompaniesHouse\Resources\Company;

/**
 * Class CompaniesHouse.
 */
class CompaniesHouse
{
    /**
     * @var
     */
    public $client;

    /**
     * CompaniesHouseApi constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $number
     * @return Company
     */
    public function company($number)
    {
        return new Company($this->client, $number);
    }

    /**
     * @return Search
     */
    public function search()
    {
        return new Search($this->client);
    }
}
