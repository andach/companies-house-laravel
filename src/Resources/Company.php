<?php

namespace Andach\CompaniesHouse\Resources;

use Andach\CompaniesHouse\Http\Client;

/**
 * Class Company.
 */
class Company extends ResourcesBase
{
    /**
     * The number of the company to search for. 
     * 
     * @var
     */
    protected $number;

    /**
     * This is the base path to add to $base_uri. For example, for Company-related queries, this is 
     * "/company/{number}", and is set in the individual file that extended ResourcesBase.php
     * 
     * @var string
     */
    protected $baseApiPath;

    /**
     * Company constructor.
     * @param Client $client
     * @param $number
     */
    public function __construct(Client $client, $number)
    {
        parent::__construct($client);

        // Account smoothly for people not putting in leading zeroes. 
        if (strlen($number) < 8)
        {
            $number = str_pad($number, 8, '0', STR_PAD_LEFT);
        }

        $this->number = trim($number);

        $this->baseApiPath = '/company/'.$this->number;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function get()
    {
        return $this->returnResponse('');
    }

    /**
     * @return array|mixed|null|object
     * @throws \Exception
     */
    public function officers()
    {
        return $this->returnResponse('/officers');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function registeredOfficeAddress()
    {
        return $this->returnResponse('/registered-office-address');
    }

    /**
     * @return array|mixed|null|object
     * @throws \Exception
     */
    public function ukEstablishments()
    {
        return $this->returnResponse('/uk-establishments');
    }

    /**
     * Returns a standard object of all charges as received by companies house, or null if no charges exist. 
     * 
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     */
    public function listCharges($items_per_page = 20, $start_index = 0)
    {
        $params = [
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];
        
        return $this->returnResponse('/charges', $params);
    }

    /**
     * Returns a standard object formatted as received from companies house given a chargeID, or null if it doesn't exist. 
     * 
     * @param $chargeId
     * @return array|mixed|null|object
     */
    public function findCharge($chargeId)
    {
        if (empty($chargeId)) {
            throw new \InvalidArgumentException('You must provide a ChargesId.');
        }

        return $this->returnResponse('/charges/'.$chargeId);
    }

    /**
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     */
    public function listFilingHistory($items_per_page = 20, $start_index = 0)
    {
        $params = [
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];

        return $this->returnResponse('/filing-history', $params);
    }

    /**
     * @param $transaction_id
     * @return array|mixed|null|object
     */
    public function findFilingTransaction($transaction_id)
    {
        if (empty($transaction_id)) {
            throw new InvalidArgumentException('You must provide a transactionId.');
        }

        return $this->returnResponse('/filing-history/'.$transaction_id);
    }
}
