<?php

namespace Andach\CompaniesHouse\Resources;

use Andach\CompaniesHouse\Exceptions\InvalidResourceException;
use Andach\CompaniesHouse\Http\Client;

/**
 * Class ResourcesBase.
 */
class ResourcesBase
{
    /**
     * @var
     */
    protected $client;

    /**
     * This is the basic domain name of the companies house API, set in the config.php page. 
     * 
     * @var string
     */
    protected $base_uri = 'https://api.companieshouse.gov.uk';

    /**
     * This is the base path to add to $base_uri. For example, for Company-related queries, this is 
     * "/company/{number}", and is set in the individual file that extended ResourcesBase.php
     * 
     * @var string
     */
    protected $baseApiPath;

    /**
     * ResourcesBase constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $response
     *
     * @return array|mixed|null|object
     *
     * @throws \Exception
     */
    protected function response($response)
    {
        if (empty($response)) {
            throw new \Exception('Invalid response to extract data from.');
        }

        return json_decode($response);
    }

    /**
     * @param $uri
     * @return string
     */
    protected function buildResourceUrl($uri)
    {
        return $this->base_uri.$uri;
    }

    /**
     * Function to build and simplify the returning of responses. 
     *
     * @param [type] $url
     * @param array $params
     * @return void
     */
    public function returnResponse($uri = '', $params = [])
    {
        try
        {
            $url = $this->buildResourceUrl($this->baseApiPath.$uri);

            return $this->client->get($url, $params);
        } catch (InvalidResourceException $e) {
            // Companies House returns a 404 error when a company has no charges. Helpful. So lets return null to be 
            // helpful to the user. 
            
            return null;
        }
}
}
