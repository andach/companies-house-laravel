<?php

namespace Andach\CompaniesHouse\Resources;


use Andach\CompaniesHouse\Http\Client;

class Search extends ResourcesBase
{

    /**
     * This is the base path to add to $base_uri. For example, for Company-related queries, this is 
     * "/company/{number}", and is set in the individual file that extended ResourcesBase.php
     * 
     * @var string
     */
    protected $baseApiPath;

    /**
     * Company constructor.
     * @param Client $client
     * @param $number
     */
    public function __construct(Client $client)
    {
        parent::__construct($client);

        $this->baseApiPath = '/search';
    }

    /**
     * @param $keyword
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     * @internal param $name
     */
    public function all($keyword, $items_per_page = 20, $start_index = 0)
    {
        $params = [
            'q' => $keyword,
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];

        return $this->returnResponse('', $params);
    }

    /**
     * @param $keyword
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     * @internal param $name
     */
    public function companies($keyword, $items_per_page = 20, $start_index = 0)
    {
        $params = [
            'q' => $keyword,
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];

        return $this->returnResponse('/companies', $params);
    }

    /**
     * @param $keyword
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     * @internal param $name
     */
    public function officers($keyword, $items_per_page = 20, $start_index = 0)
    {
        $params = [
            'q' => $keyword,
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];
        
        return $this->returnResponse('/officers', $params);
    }

    /**
     * @param $keyword
     * @param int $items_per_page
     * @param int $start_index
     * @return array|mixed|null|object
     * @internal param $name
     */
    public function disqualified_officers($keyword, $items_per_page = 20, $start_index = 0)
    {
        $params = [
            'q' => $keyword,
            'items_per_page' => $items_per_page,
            'start_index' => $start_index,
        ];
        
        return $this->returnResponse('/disqualified-officers', $params);
    }
}
