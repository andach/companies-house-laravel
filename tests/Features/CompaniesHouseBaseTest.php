<?php

namespace Andach\CompaniesHouse\Tests\Features;

use PHPUnit\Framework\TestCase;
use Andach\CompaniesHouse\Http\Client;
use Andach\CompaniesHouse\CompaniesHouse;

abstract class CompaniesHouseBaseTest extends TestCase
{
    /**
     * @var string
     */
    protected $api_key = 'HVPuwD7am9jpn-BH9f3Mo-YS9phoKrn9DT0zdKtf';

    /**
     * @var string
     */
    protected $base_uri = 'https://api.companieshouse.gov.uk/';

    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $api;

    /**
     * @test
     */
    public function setUp()
    {
        parent::setUp();

        $this->client = new Client($this->base_uri, $this->api_key);

        $this->api = new CompaniesHouse($this->client);

        $this->assertTrue(true);
    }
}
