<?php

namespace Andach\CompaniesHouse\Tests\Features;

class CompanyTest extends CompaniesHouseBaseTest
{
    /**
     * Denotes the registered number of a relevant, live company. 
     * 
     * @var string
     */
    protected $number = '07257056';

    /**
     * Denotes the registered number of a company which has closed and whose details will never change. 
     *
     * @var string
     */
    protected $deadNumber = '09804638';

    /**
     * @test
     */
    public function get()
    {
        $company = $this->api->company($this->deadNumber)->get();

        $this->assertEquals($company->company_name, 'CAN I USE A QUESTION MARK IN A COMPANY NAME? LTD');
        $this->assertEquals($company->registered_office_address->address_line_1, '160 Knighton Fields Road East');
    }

    /**
     * @test
     */
    public function registeredOfficeAddress()
    {
        $address = $this->api->company($this->deadNumber)->registeredOfficeAddress();

        $this->assertEquals($address->address_line_1, '160 Knighton Fields Road East');
        $this->assertEquals($address->country, 'United Kingdom');
        $this->assertEquals($address->locality, 'Leicester');
        $this->assertEquals($address->postal_code, 'LE2 6DR');
    }

    /**
     * @test
     */
    public function officers()
    {
        $officers = $this->api->company($this->deadNumber)->officers();

        $this->assertEquals(count($officers->items), 2);
        $this->assertEquals($officers->active_count, 0);
        $this->assertEquals($officers->items[0]->name, 'CHRISTODOULOU, Andreas');
    }

    /**
     * @test
     */
    public function ukEstablishments()
    {
        $establishments = $this->api->company($this->number)->ukEstablishments();

        $this->assertEquals($establishments->etag, '507ef9889d3ece2101336d501569b0a10284ed2a');
        $this->assertEquals($establishments->kind, 'related-companies');
    }

    /**
     * @test
     */
    public function listFilingHistory()
    {
        //First check the whole filing history. 
        $history = $this->api->company($this->deadNumber)->listFilingHistory();
        
        $this->assertEquals(count($history->items), 8);
        $this->assertEquals($history->items[0]->type, 'GAZ2(A)');
        $this->assertEquals($history->items[0]->date, '2017-09-26');
        $this->assertEquals($history->items[0]->category, 'gazette');

        //Then get just two items. 
        $history = $this->api->company($this->deadNumber)->listFilingHistory(2);
        
        $this->assertEquals(count($history->items), 2);
        $this->assertEquals($history->items[0]->type, 'GAZ2(A)');
        $this->assertEquals($history->items[0]->date, '2017-09-26');
        $this->assertEquals($history->items[0]->category, 'gazette');

        //Then get just the fifth and sixth items. 
        $history = $this->api->company($this->deadNumber)->listFilingHistory(2, 5);
        
        $this->assertEquals(count($history->items), 2);
        $this->assertEquals($history->items[0]->type, 'CS01');
        $this->assertEquals($history->items[0]->date, '2016-10-28');
        $this->assertEquals($history->items[0]->category, 'confirmation-statement');
    }

    /**
     * @test
     */
    public function findFilingTransaction()
    {
        $transactionId = 'MzIyOTkzMjI0N2FkaXF6a2N4';

        // Check a valid filing transaction.
        $transaction = $this->api->company('07257056')->findFilingTransaction($transactionId);

        $this->assertEquals($transaction->transaction_id, $transactionId);
        $this->assertEquals($transaction->description, 'accounts-with-accounts-type-full');
        $this->assertEquals($transaction->action_date, '2018-12-31');

        // Check an invalid filing transaction, which should return null.
        $transaction = $this->api->company('07257056')->findFilingTransaction('invalid');

        $this->assertEquals($transaction, null);
    }

    /**
     * @test
     */
    public function listCharges()
    {
        // Test for a company with known charges. This should return the standard object. 
        $charges = $this->api->company('07257056')->listCharges();

        $this->assertArrayHasKey('unfiltered_count', (array) $charges);
        $this->assertArrayHasKey('delivered_on', (array) $charges->items[0]);
        $this->assertArrayHasKey('persons_entitled', (array) $charges->items[0]);

        // Now check that searching with leading or lagging empty spaces works. 
        $charges = $this->api->company(' 07257056 ')->listCharges();

        $this->assertArrayHasKey('unfiltered_count', (array) $charges);
        $this->assertArrayHasKey('delivered_on', (array) $charges->items[0]);
        $this->assertArrayHasKey('persons_entitled', (array) $charges->items[0]);

        // Similarly that dropping the leading zero works. 
        $charges = $this->api->company('7257056')->listCharges();

        $this->assertArrayHasKey('unfiltered_count', (array) $charges);
        $this->assertArrayHasKey('delivered_on', (array) $charges->items[0]);
        $this->assertArrayHasKey('persons_entitled', (array) $charges->items[0]);

        // Finally check a company known to have no charges. Check this returns null. 
        $charges = $this->api->company('09804638')->listCharges();

        $this->assertEquals($charges, null);

        // Finally check an invalid company. 
        $charges = $this->api->company('invalid')->listCharges();

        $this->assertEquals($charges, null);
    }

    /**
     * @test
     */
    public function findCharge()
    {
        // Check for a known charge for the correct company. 
        $charges = $this->api->company('07257056')->findCharge('dJc5r4NI1d0nC3lPHoQ0ocSJGXY');

        $this->assertArrayHasKey('charge_number', (array) $charges);
        $this->assertArrayHasKey('transactions', (array) $charges);
        $this->assertArrayHasKey('particulars', (array) $charges);

        // Check for a known charge for the wrong company. 
        $charges = $this->api->company('09804638')->findCharge('dJc5r4NI1d0nC3lPHoQ0ocSJGXY');
        
        $this->assertEquals($charges, null);

        // Check for a completely invalid charge. 
        $charges = $this->api->company('07257056')->findCharge('invalid');
        
        $this->assertEquals($charges, null);
    }
}
