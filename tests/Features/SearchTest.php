<?php

namespace Andach\CompaniesHouse\Tests\Features;

class SearchTest extends CompaniesHouseBaseTest
{
    /**
     * @test
     */
    public function search_all()
    {
        // Do a basic test of functionality. 
        $companies = $this->api->search()->all('Can I Use A Question Mark');

        $this->assertEquals(count($companies->items), 20);
        $this->assertArrayHasKey('address_snippet', (array) $companies->items[0]);
        $this->assertArrayHasKey('total_results', (array) $companies);

        // Now check the limiter works. 
        $companies = $this->api->search()->all('Can I Use A Question Mark', 10);

        $this->assertEquals(count($companies->items), 10);
        $this->assertArrayHasKey('address_snippet', (array) $companies->items[0]);
        $this->assertArrayHasKey('total_results', (array) $companies);

        // Check this gives nothing. 
        $companies = $this->api->search()->all('isdunbviuysnbd87undfivubdifuygvbd');

        $this->assertEquals(count($companies->items), 0);
    }

    /**
     * @test
     */
    public function search_by_company_name()
    {
        $name = 'Can I Use A Question Mark';

        $companies = $this->api->search()->companies($name);

        $this->assertArrayHasKey('address_snippet', (array) $companies->items[0]);
    }

    /**
     * @test
     */
    public function search_officers_by_name()
    {
        $officer = 'Andreas Christodoulou';

        $officers = $this->api->search()->officers($officer);

        $this->assertArrayHasKey('title', (array) $officers->items[0]);
    }

    /**
     * @test
     */
    public function search_disqualified_officers_by_name()
    {
        $name = 'Andreas Christodoulou';

        $officers = $this->api->search()->disqualified_officers($name);

        $this->assertArrayHasKey('title', (array) $officers->items[0]);
        $this->assertArrayHasKey('kind', (array) $officers->items[0]);
        $this->assertArrayHasKey('address', (array) $officers->items[0]);
    }
}
